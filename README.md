# Markov Chains #
A Markov chain is a stochastic model describing a sequence of possible events in which the probability of each event depends only on the state attained in the previous event.

This library allows one to train markov chains on generic sequences (for example Character, String, Integer,...). Use this trained model to: 

 * determine probabilities of unknown sequences. 
 * generate new random sequences.



## Example ##
```java

class Demo { 
    
    
    public static void main(String[] args){
        static String text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
        
        MarkovChain<Character> markovChain = new MarkovChain<>();
        markovChain.train(n, characters(text));
        System.out.println("prob(\"ipsum\", 4): " + markovChain.probability(4, characters("ipsum")));
        System.out.println("prob(\"ipsum\", 2): " + markovChain.probability(2, characters("ipsum")));
        System.out.println("prob(\"arvid\", 2): " + markovChain.probability(2, characters("arvid")));
        
        System.out.println("random: " + string(markovChain.generate(new Random(42), 160)));
    }
    
    static Character[] characters(String s) {
        Character[] result = new Character[s.length()];
        for (int i = 0; i < result.length; i++) {
             result[i] = s.charAt(i);
        }
        return result;
    }

    static String string(List<Character> cs) {
        char[] result = new char[cs.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = cs.get(i);
        }
        return new String(result);
    }
}
```

## Credits ##
Arvid Halma (HumanityX, Centre for Innovation, Leiden University)

## License ##
Copyright 2018 Centre for Innovation, Leiden University

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.